import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(XylophoneApp());
}

final player = AudioCache();

class XylophoneApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Center(child: getKeyboard()),
        ),
      ),
    );
  }

  Widget getKeyboard() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        buildKey(1, Colors.red, "do"),
        buildKey(2, Colors.deepOrangeAccent, "re"),
        buildKey(3, Colors.yellow, "mi"),
        buildKey(4, Colors.green[800], "fa"),
        buildKey(5, Colors.tealAccent[200], "so"),
        buildKey(6, Colors.lightBlue, "la"),
        buildKey(7, Colors.deepPurple, "ti"),
        // buildKey(8, Colors.pinkAccent, "do"),
      ],
    );
  }

  Widget buildKey(int i, Color color, String keyName) {
    return Expanded(
      child: FlatButton(
        color: color,
        onPressed: () {
          player.play("note$i.wav");
        },
        child: Text(
          keyName,
          style: TextStyle(
              color: Colors.white,
              decoration: TextDecoration.combine(
                  [TextDecoration.underline, TextDecoration.overline])),
        ),
      ),
    );
  }
}
